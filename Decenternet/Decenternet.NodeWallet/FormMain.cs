﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Decenternet.NodeWallet
{
    public partial class frmMain : Form
    {
        #region Members

        private bool _dragging;
        private Point _offset;

        #endregion

        #region Form Events     

        public frmMain()
        {
            InitializeComponent();
        }

        private void frmMain_MouseMove(object sender, MouseEventArgs e)
        {
            if (_dragging)
            {
                Point currentScreenPos = PointToScreen(e.Location);
                this.Location = new Point(currentScreenPos.X - this._offset.X, currentScreenPos.Y - this._offset.Y);
            }
        }

        private void frmMain_MouseDown(object sender, MouseEventArgs e)
        {
            this._offset.X = e.X;
            this._offset.Y = e.Y;
            this._dragging = true;
        }

        private void frmMain_MouseUp(object sender, MouseEventArgs e)
        {
            _dragging = false;
        }

        #endregion

        #region Form Control Events

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnStartMining_Click(object sender, EventArgs e)
        {

        }

        #endregion


    }
}
