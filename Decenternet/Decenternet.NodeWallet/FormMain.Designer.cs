﻿namespace Decenternet.NodeWallet
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.btnClose = new System.Windows.Forms.Button();
            this.btnMinimize = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnStartMining = new System.Windows.Forms.Button();
            this.btnDashboard = new System.Windows.Forms.Button();
            this.btnDNS = new System.Windows.Forms.Button();
            this.btnUtilities = new System.Windows.Forms.Button();
            this.btnTransactions = new System.Windows.Forms.Button();
            this.btnNetwork = new System.Windows.Forms.Button();
            this.btnNotification = new System.Windows.Forms.Button();
            this.btnStorage = new System.Windows.Forms.Button();
            this.btnSetttings = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnClose
            // 
            this.btnClose.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnClose.FlatAppearance.BorderSize = 0;
            this.btnClose.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClose.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnClose.Location = new System.Drawing.Point(746, -1);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(46, 33);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "X";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // btnMinimize
            // 
            this.btnMinimize.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnMinimize.FlatAppearance.BorderSize = 0;
            this.btnMinimize.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMinimize.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnMinimize.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMinimize.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnMinimize.Location = new System.Drawing.Point(701, -1);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(46, 33);
            this.btnMinimize.TabIndex = 1;
            this.btnMinimize.Text = "_";
            this.btnMinimize.UseVisualStyleBackColor = true;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Decenternet.NodeWallet.Properties.Resources.decentenet_logo_node_wallet;
            this.pictureBox1.Location = new System.Drawing.Point(6, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(295, 62);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnStartMining);
            this.panel1.Location = new System.Drawing.Point(148, 62);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(643, 410);
            this.panel1.TabIndex = 3;
            // 
            // btnStartMining
            // 
            this.btnStartMining.BackColor = System.Drawing.Color.Orange;
            this.btnStartMining.FlatAppearance.MouseDownBackColor = System.Drawing.Color.DarkOrange;
            this.btnStartMining.FlatAppearance.MouseOverBackColor = System.Drawing.Color.DarkOrange;
            this.btnStartMining.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStartMining.Font = new System.Drawing.Font("Berlin Sans FB", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStartMining.ForeColor = System.Drawing.SystemColors.InfoText;
            this.btnStartMining.Location = new System.Drawing.Point(511, 359);
            this.btnStartMining.Name = "btnStartMining";
            this.btnStartMining.Size = new System.Drawing.Size(111, 31);
            this.btnStartMining.TabIndex = 10;
            this.btnStartMining.Text = "START";
            this.btnStartMining.UseVisualStyleBackColor = false;
            this.btnStartMining.Click += new System.EventHandler(this.btnStartMining_Click);
            // 
            // btnDashboard
            // 
            this.btnDashboard.BackColor = System.Drawing.Color.White;
            this.btnDashboard.FlatAppearance.BorderSize = 0;
            this.btnDashboard.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnDashboard.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDashboard.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDashboard.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.btnDashboard.Location = new System.Drawing.Point(1, 62);
            this.btnDashboard.Name = "btnDashboard";
            this.btnDashboard.Size = new System.Drawing.Size(147, 38);
            this.btnDashboard.TabIndex = 4;
            this.btnDashboard.Text = "Dashboard";
            this.btnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.UseVisualStyleBackColor = false;
            // 
            // btnDNS
            // 
            this.btnDNS.FlatAppearance.BorderSize = 0;
            this.btnDNS.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnDNS.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnDNS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDNS.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDNS.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnDNS.Location = new System.Drawing.Point(1, 218);
            this.btnDNS.Name = "btnDNS";
            this.btnDNS.Size = new System.Drawing.Size(147, 38);
            this.btnDNS.TabIndex = 5;
            this.btnDNS.Text = "Domain Name Service";
            this.btnDNS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDNS.UseVisualStyleBackColor = true;
            // 
            // btnUtilities
            // 
            this.btnUtilities.FlatAppearance.BorderSize = 0;
            this.btnUtilities.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnUtilities.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnUtilities.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUtilities.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnUtilities.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnUtilities.Location = new System.Drawing.Point(1, 179);
            this.btnUtilities.Name = "btnUtilities";
            this.btnUtilities.Size = new System.Drawing.Size(147, 38);
            this.btnUtilities.TabIndex = 6;
            this.btnUtilities.Text = "Utilities";
            this.btnUtilities.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUtilities.UseVisualStyleBackColor = true;
            // 
            // btnTransactions
            // 
            this.btnTransactions.FlatAppearance.BorderSize = 0;
            this.btnTransactions.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnTransactions.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnTransactions.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransactions.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransactions.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnTransactions.Location = new System.Drawing.Point(1, 101);
            this.btnTransactions.Name = "btnTransactions";
            this.btnTransactions.Size = new System.Drawing.Size(147, 38);
            this.btnTransactions.TabIndex = 7;
            this.btnTransactions.Text = "Transactions";
            this.btnTransactions.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnTransactions.UseVisualStyleBackColor = true;
            // 
            // btnNetwork
            // 
            this.btnNetwork.FlatAppearance.BorderSize = 0;
            this.btnNetwork.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnNetwork.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnNetwork.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNetwork.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNetwork.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnNetwork.Location = new System.Drawing.Point(1, 140);
            this.btnNetwork.Name = "btnNetwork";
            this.btnNetwork.Size = new System.Drawing.Size(147, 38);
            this.btnNetwork.TabIndex = 8;
            this.btnNetwork.Text = "Network";
            this.btnNetwork.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnNetwork.UseVisualStyleBackColor = true;
            // 
            // btnNotification
            // 
            this.btnNotification.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNotification.FlatAppearance.BorderSize = 0;
            this.btnNotification.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnNotification.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(89)))), ((int)(((byte)(89)))), ((int)(((byte)(89)))));
            this.btnNotification.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNotification.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNotification.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.btnNotification.Image = global::Decenternet.NodeWallet.Properties.Resources.notification;
            this.btnNotification.Location = new System.Drawing.Point(649, -1);
            this.btnNotification.Name = "btnNotification";
            this.btnNotification.Size = new System.Drawing.Size(46, 33);
            this.btnNotification.TabIndex = 9;
            this.btnNotification.UseVisualStyleBackColor = true;
            // 
            // btnStorage
            // 
            this.btnStorage.FlatAppearance.BorderSize = 0;
            this.btnStorage.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnStorage.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnStorage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStorage.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStorage.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnStorage.Location = new System.Drawing.Point(1, 257);
            this.btnStorage.Name = "btnStorage";
            this.btnStorage.Size = new System.Drawing.Size(147, 38);
            this.btnStorage.TabIndex = 11;
            this.btnStorage.Text = "Storage";
            this.btnStorage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStorage.UseVisualStyleBackColor = true;
            // 
            // btnSetttings
            // 
            this.btnSetttings.FlatAppearance.BorderSize = 0;
            this.btnSetttings.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnSetttings.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnSetttings.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetttings.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSetttings.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.btnSetttings.Location = new System.Drawing.Point(1, 432);
            this.btnSetttings.Name = "btnSetttings";
            this.btnSetttings.Size = new System.Drawing.Size(147, 38);
            this.btnSetttings.TabIndex = 10;
            this.btnSetttings.Text = "Settings";
            this.btnSetttings.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetttings.UseVisualStyleBackColor = true;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(68)))), ((int)(((byte)(68)))));
            this.ClientSize = new System.Drawing.Size(792, 473);
            this.Controls.Add(this.btnStorage);
            this.Controls.Add(this.btnSetttings);
            this.Controls.Add(this.btnNotification);
            this.Controls.Add(this.btnNetwork);
            this.Controls.Add(this.btnTransactions);
            this.Controls.Add(this.btnUtilities);
            this.Controls.Add(this.btnDNS);
            this.Controls.Add(this.btnDashboard);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnMinimize);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Decenternet - Node Wallet";
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseDown);
            this.MouseMove += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseMove);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.frmMain_MouseUp);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.Button btnMinimize;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnDashboard;
        private System.Windows.Forms.Button btnDNS;
        private System.Windows.Forms.Button btnUtilities;
        private System.Windows.Forms.Button btnTransactions;
        private System.Windows.Forms.Button btnNetwork;
        private System.Windows.Forms.Button btnNotification;
        private System.Windows.Forms.Button btnStartMining;
        private System.Windows.Forms.Button btnStorage;
        private System.Windows.Forms.Button btnSetttings;
    }
}

