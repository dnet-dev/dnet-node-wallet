# node-wallet

<h3>Specifications</h3>
<ul>
<li>Ability to earn and store Spice.</li>
<li>A real-time network traffic monitoring graph for all incoming and outgoing fragments.</li>
<li>Network details such as current speed and latency.</li>
<li>Display of remaining available storage allotted for fragments.</li>
<li>Intuitive application used by the nodes to make Spice transfer transactions. </li>
<li>Spice transaction history scan. </li>
<li>Daily settings where nodes can define day-to-day service schemes and schedule, such as bandwidth to be used for a scheduled date and time.</li>
<li>The amount of storage to be used in hosting contents.</li>
<li>A cleaner that removes 0 momentum fragments that are not intended to be kept in a high score PoR node. The cleaner will transfer the deleted fragments to other nodes to maintain its minimum copy.</li>

<ul>
